<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tourismes extends Model
{
    protected $fillable = [
        'titre_Tourisme',
        'image_Tourisme',
        'description_tourisme',
        'lien_tourisme'
    ];

    public function pays()
    {
        return $this->belongsTo(Pays::class);
    }
}
