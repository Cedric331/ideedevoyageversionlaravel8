<?php

namespace App\Http\Controllers;

use App\Models\User as user;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;


class CompteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Retourne la vue du compte de l'utilisateur
     *
     * @return void
     */
    public function index()
    {
        return view('dashboard');
    }


    /**
     * @param [type] $id
     * Suppression de l'utilisateur avec message de confirmation sur la page d'accueil
     * @return void
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return redirect('accueil')->with('message', 'Votre compte a bien été supprimé !');
    }

   /**
    * @param Request $request
    * Upload de l'avatar de l'utilisateur avec contrôle de l'avatar envoyé par l'utilisateur
    * @return void
    */
    public function avatarUpdate(Request $request, $id)
    {

        $request->validate([
            'avatar' => 'required|image|mimes:jpeg,png'
        ]);

        $user = user::find($id);
        $path = $request->file('avatar')->store('avatars/'.$user->email, 'public');
        $user->avatar = $path;
        $user->save();

        return redirect()->back()->with('message', 'Avatar à jour !');
    }

/**
 * Retourne le formulaire de mise à jour de l'adresse email
 *
 * @return void
 */
    public function emailUpdateForm()
    {
        return view('auth.compte-email');
    }


    /**
     * Mise à jour de l'adresse email de l'utilisateur après vérification des éléments
     *
     * @param Request $request
     * @return void
     */
    public function emailUpdate(Request $request, $id)
    {

        $request->validate([
        'emailUpdate' => ['required', 'string', 'email', 'max:100', 'unique:users,email']
        ]);

        $user = User::find($id);
        $user->email = $request->emailUpdate;
        $user->save();

        return redirect('compte')->with('message', 'Adresse email modifiée!');


    }
}
