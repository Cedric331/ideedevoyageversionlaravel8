@extends('layouts/app')


@section('fond', 'france')

@section('content')
<div class="container cadre">
    <div class="row flex wrap">
        <div class="col-lg-6 centre">
            <img class="imagePays" src="image/pays/france/toureiffel1.jpg" alt="image de la tour eiffel">
        </div>
        <div class="col-lg-6 col-md-12">
            <h1>France</h1>
            <h4>Budget moyen: 599 € / semaine</h4>
            <p>La France présente de nombreux attraits tant naturels que culturels et historiques. Ouverte sur la mer et
                l’océan, animée par de nombreux massifs montagneux, parsemée de villages typiques et préservés, elle
                n’est
                pas pour rien le pays le plus visité au monde.
                Paris à lui seul demande plusieurs jours d’excursion : ses
                monuments, de la Tour Eiffel à l’Opéra en passant par l’Arc de triomphe et Notre-Dame, ou le Sacré-Cœur,
                pour n’en citer que quelques-uns, ses musées, du Louvre à la Cité des Sciences et au Centre Georges
                Pompidou, ses vieux quartiers, du Marais à Montmartre, ses jardins, des Tuileries au Luxembourg, ses
                boutiques de luxe, ses grands magasins, vous donneront l’occasion de flâner et de faire des découvertes
                passionnantes au gré de votre curiosité et de vos humeurs, sans jamais être déçu.

                Le pays est également réputé pour ses vins et sa cuisine raffinée. Les peintures rupestres des grottes
                de
                Lascaux,
                le théâtre romain de Lyon et l'immense château de Versailles témoignent de sa riche histoire.
            </p>
        </div>
    </div>


    <div class="mt-3 accordion" id="accordionExample">
        <div class="card">
            <div class="card-header" id="headingOne">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                        Le climat en France
                    </button>
                </h2>
            </div>
            <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                <div class="card-body">
                    La France bénéficie d’un climat tempéré, influencé par les entrées maritimes sur la côte ouest, plus
                    continental dans le centre et l’est, et de type méditerranéen au sud et bien évidemment sur la côte
                    méditerranéenne.

                    Les saisons sont en général très marquées, avec des hivers froids, des étés chauds et plus secs, et
                    des
                    intersaisons bien nettes et ponctuées d’épisodes pluvieux. La géographie accidentée du territoire
                    ajoute
                    à ce climat général des variations locales : hivers rigoureux en montagne, printemps et automne plus
                    doux
                    et ensoleillés sur les côtes méditerranéenne et atlantique, précipitations anarchiques sur la pointe
                    bretonne, étés quelquefois caniculaires en Alsace par exemple.
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="headingTwo">
                <h2 class="mb-0">
                    <button class="btn btn-link btn-block collapsed" type="button" data-toggle="collapse"
                        data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Meilleure période pour la France ?
                    </button>
                </h2>
            </div>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                <div class="card-body">
                   La meilleure période pour visiter la France court du milieu du printemps au début de l'automne, de mai à octobre. Selon les régions, les mois de juillet-août peuvent se révéler encombrés.
                </div>
            </div>
        </div>
    </div>

</div>



    <div class="card-deck ml-2 mr-2">
        <div class="card">
            <img src="/image/pays/france/toureiffel.jpg" class="card-img-top" alt="Image de la tour eiffel">
            <div class="card-body">
                <h5 class="card-title">La Tour Eiffel</h5>
                <p class="card-text">Symbole de Paris et plus largement de France, la tour Eiffel fut
                    construite par
                    Gustave Eiffel à l’occasion de l’Exposition universelle de 1889. Elle fait partie des monuments les
                    plus
                    visités au monde.</p>
                <a href="https://www.toureiffel.paris/fr" class="btn btn-primary" target="blank">En savoir plus</a>
            </div>
        </div>
        <div class="card">
            <img src="/image/pays/france/louvre.jpg" class="card-img-top" alt="Photo du Louvre">
            <div class="card-body">
                <h5 class="card-title">Le Louvre</h5>
                <p class="card-text">Chaque passage dans la capitale mérite une visite du Louvre, tant le plus grand
                    musée de Paris recèle de trésors.Au total, quelque 35 000 œuvres ! En huit siècles d’existence, le
                    Louvre a été marqué par de nombreux évènements.</p>
                <a href="https://www.louvre.fr/" class="btn btn-primary" target="blank">En savoir plus</a>
            </div>
        </div>
        <div class="card">
            <img src="/image/pays/france/triomphe.jpg" class="card-img-top" alt="Photo des Champs-Élysées">
            <div class="card-body">
                <h5 class="card-title">Les Champs-Élysées</h5>
                <p class="card-text">L’avenue des Champs-Élysées est une voie de Paris. Site touristique majeur, elle a
                    souvent passé pour la plus belle avenue de la capitale, et est connue en France comme la « plus
                    belle avenue du monde.</p>
                <a href="https://www.louvre.fr/" class="btn btn-primary" target="blank">En savoir plus</a>
            </div>
        </div>
    </div>




@guest
<div class="container cadre centre">
    <p>Pour partager vos impressions sur ce Pays, vous devez être connecté</p>
</div>
@else
<div class="container cadre centre">
    <form>
        <div class="form-group">
                <div>
                    <label for="commentaire">Un avis? Un témoignage? N'hésiter pas à partager avec nous </label>
                </div>
                <div>
                    <textarea name="commentaire" id="commentaire" cols="60" rows="10"></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Commenter</button>
        </div>
    </form>
</div>
@endguest

@endsection
